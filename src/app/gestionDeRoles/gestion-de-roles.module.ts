import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionDeRolesRoutingModule } from './gestion-de-roles-routing.module';
import { GestionDeRolesComponent } from './gestion-de-roles.component';
import {NavbarModule} from '../navbar/navbar.module';

@NgModule({
  declarations: [GestionDeRolesComponent],
  imports: [
    CommonModule,
    GestionDeRolesRoutingModule,
    NavbarModule
  ]
})
export class GestionDeRolesModule { }
