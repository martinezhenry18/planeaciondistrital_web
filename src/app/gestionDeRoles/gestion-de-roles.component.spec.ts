import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionDeRolesComponent } from './gestion-de-roles.component';

describe('GestionDeRolesComponent', () => {
  let component: GestionDeRolesComponent;
  let fixture: ComponentFixture<GestionDeRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionDeRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionDeRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
