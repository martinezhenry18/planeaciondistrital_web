import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParametrizacionRoutingModule } from './parametrizacion-routing.module';
import {ParametrizacionComponent} from './parametrizacion.component';
import {NavbarModule} from '../navbar/navbar.module';



@NgModule({
  declarations: [ParametrizacionComponent],
  imports: [
    CommonModule,
    ParametrizacionRoutingModule,
    NavbarModule
  ],


})
export class ParametrizacionModule { }
