import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import {HomeComponent} from './home.component';
import {NavbarModule} from '../navbar/navbar.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';



@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    NavbarModule,
    NgbModule,
    FontAwesomeModule,

  ],
  declarations: [
    HomeComponent],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
