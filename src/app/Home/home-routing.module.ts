import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {ParametrizacionComponent} from '../parametrizacion/parametrizacion.component';
import {BateriaDeIndicadoresComponent} from '../bateriaDeIndicadores/bateria-de-indicadores.component';
import {GestionDeRolesComponent} from '../gestionDeRoles/gestion-de-roles.component';
import {InformesDeSeguimientoComponent} from '../informesDeSeguimiento/informes-de-seguimiento.component';
import {PlanDeAccionComponent} from '../planDeAccion/plan-de-accion.component';
import {PoliticaPublicaComponent} from '../politicaPublica/politica-publica.component';
import {RegistroDeSeguimientoComponent} from '../registroDeSeguimiento/registro-de-seguimiento.component';

const routes: Routes = [

  {path: 'parametrizacion', component: ParametrizacionComponent, data: {title: 'Parametrizacion'}},
  {path: 'bateriaDeIndicadores', component: BateriaDeIndicadoresComponent, data: {title: 'Bateria de Indicadores'}},
  {path: 'gestionDeRoles', component: GestionDeRolesComponent, data: {title: 'Gestion de Roles'}},
  {path: 'informesDeSeguimiento', component: InformesDeSeguimientoComponent, data: {title: 'Informes de Seguimiento'}},
   {path: 'planDeAccion', component: PlanDeAccionComponent, data: {title: 'Plan de Accion'}},
  {path: 'politicaPublica', component: PoliticaPublicaComponent, data: {title: 'Politica Publica'}},
  {path: 'regristroDeSeguimiento', component: RegistroDeSeguimientoComponent, data: {title: 'Registro de Seguimiento'}},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
