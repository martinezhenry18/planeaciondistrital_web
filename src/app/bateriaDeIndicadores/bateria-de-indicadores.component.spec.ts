import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BateriaDeIndicadoresComponent } from './bateria-de-indicadores.component';

describe('BateriaDeIndicadoresComponent', () => {
  let component: BateriaDeIndicadoresComponent;
  let fixture: ComponentFixture<BateriaDeIndicadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BateriaDeIndicadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BateriaDeIndicadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
