import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BeteriaDeIndicadoresRoutingModule } from './beteria-de-indicadores-routing.module';
import {NavbarModule} from '../navbar/navbar.module';
import {BateriaDeIndicadoresComponent} from './bateria-de-indicadores.component';

@NgModule({
  declarations: [BateriaDeIndicadoresComponent],
  imports: [
    CommonModule,
    BeteriaDeIndicadoresRoutingModule,
    NavbarModule
  ]
})
export class BeteriaDeIndicadoresModule { }
