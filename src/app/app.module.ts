import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeModule } from './Home/home.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NavbarModule} from './navbar/navbar.module';
import {ParametrizacionModule} from './parametrizacion/parametrizacion.module';
import {BeteriaDeIndicadoresModule} from './bateriaDeIndicadores/beteria-de-indicadores.module';
import {InformesDeSeguimientoModule} from './informesDeSeguimiento/informes-de-seguimiento.module';
import {PlanDeAccionModule} from './planDeAccion/plan-de-accion.module';
import {PoliticaPublicaModule} from './politicaPublica/politica-publica.module';
import {RegistroDeSeguimientoModule} from './registroDeSeguimiento/registro-de-seguimiento.module';
import {GestionDeRolesModule} from './gestionDeRoles/gestion-de-roles.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    AppComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HomeModule,
    NavbarModule,
    FontAwesomeModule,
    ParametrizacionModule,
    BeteriaDeIndicadoresModule,
    InformesDeSeguimientoModule,
    PlanDeAccionModule,
    PoliticaPublicaModule,
    RegistroDeSeguimientoModule,
    GestionDeRolesModule,
    NgbModule,

  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule {

}
