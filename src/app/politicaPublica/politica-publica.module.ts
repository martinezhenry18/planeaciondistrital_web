import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PoliticaPublicaRoutingModule } from './politica-publica-routing.module';
import { PoliticaPublicaComponent } from './politica-publica.component';
import {NavbarModule} from '../navbar/navbar.module';

@NgModule({
  declarations: [PoliticaPublicaComponent],
  imports: [
    CommonModule,
    PoliticaPublicaRoutingModule,
    NavbarModule
  ]
})
export class PoliticaPublicaModule { }
