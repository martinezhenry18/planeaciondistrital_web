import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanDeAccionRoutingModule } from './plan-de-accion-routing.module';
import { PlanDeAccionComponent } from './plan-de-accion.component';
import {NavbarModule} from '../navbar/navbar.module';

@NgModule({
  declarations: [PlanDeAccionComponent],
  imports: [
    CommonModule,
    PlanDeAccionRoutingModule,
    NavbarModule
  ]
})
export class PlanDeAccionModule { }
