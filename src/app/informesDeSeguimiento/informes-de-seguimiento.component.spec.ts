import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesDeSeguimientoComponent } from './informes-de-seguimiento.component';

describe('InformesDeSeguimientoComponent', () => {
  let component: InformesDeSeguimientoComponent;
  let fixture: ComponentFixture<InformesDeSeguimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformesDeSeguimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesDeSeguimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
