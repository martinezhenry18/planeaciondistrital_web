import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformesDeSeguimientoRoutingModule } from './informes-de-seguimiento-routing.module';
import { InformesDeSeguimientoComponent } from './informes-de-seguimiento.component';
import {NavbarModule} from '../navbar/navbar.module';

@NgModule({
  declarations: [InformesDeSeguimientoComponent],
  imports: [
    CommonModule,
    InformesDeSeguimientoRoutingModule,
    NavbarModule
  ]
})
export class InformesDeSeguimientoModule { }
