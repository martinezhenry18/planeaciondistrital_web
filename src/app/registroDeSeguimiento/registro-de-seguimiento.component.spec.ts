import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroDeSeguimientoComponent } from './registro-de-seguimiento.component';

describe('RegistroDeSeguimientoComponent', () => {
  let component: RegistroDeSeguimientoComponent;
  let fixture: ComponentFixture<RegistroDeSeguimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroDeSeguimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroDeSeguimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
