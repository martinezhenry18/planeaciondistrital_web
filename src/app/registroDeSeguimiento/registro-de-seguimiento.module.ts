import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistroDeSeguimientoRoutingModule } from './registro-de-seguimiento-routing.module';
import { RegistroDeSeguimientoComponent } from './registro-de-seguimiento.component';

import {NavbarModule} from '../navbar/navbar.module';

@NgModule({
  declarations: [RegistroDeSeguimientoComponent],
  imports: [
    CommonModule,
    RegistroDeSeguimientoRoutingModule,
    NavbarModule
  ]
})
export class RegistroDeSeguimientoModule { }
